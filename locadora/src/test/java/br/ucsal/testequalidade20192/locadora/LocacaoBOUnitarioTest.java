package br.ucsal.testequalidade20192.locadora;

import br.ucsal.testequalidade20192.locadora.builder.ClienteBuilder;
import br.ucsal.testequalidade20192.locadora.builder.VeiculoBuilder;
import br.ucsal.testequalidade20192.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20192.locadora.dominio.Cliente;
import br.ucsal.testequalidade20192.locadora.dominio.Locacao;
import br.ucsal.testequalidade20192.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20192.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20192.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20192.locadora.persistence.VeiculoDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ClienteDAO.class, LocacaoDAO.class, LocacaoBO.class, VeiculoDAO.class})

public class LocacaoBOUnitarioTest {

	/**
	 * Verificar se ao locar um veículo disponível para um cliente cadastrado, um
	 * contrato de locação é inserido.
	 * 
	 * Método:
	 * 
	 * public static Integer locarVeiculos(String cpfCliente, List<String> placas,
	 * Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado
	 *
	 * Observação1: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * Observação2: lembre-se de que o método locarVeiculos é um método command.
	 * 
	 * @throws Exception
	 */

	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		String cpfCliente = "888.999.000-11";
		String placa = "jpg-9988";
		List<String> placas = Arrays.asList(placa);
		Date dataLocacao = new Date();
		Integer quantidadeDiasLocados = 12;
		ArrayList<Veiculo> veiculos = new ArrayList<>();

		PowerMockito.mockStatic((ClienteDAO.class));
		Cliente cliente1 = ClienteBuilder.umCliente().comCPF(cpfCliente).build();
		PowerMockito.when(ClienteDAO.obterPorCpf(cpfCliente)).thenReturn(cliente1);

		PowerMockito.mockStatic(VeiculoDAO.class);
		Veiculo veiculo1 = VeiculoBuilder.umVeiculo().comPlaca(placa).build();
		PowerMockito.when(VeiculoDAO.obterPorPlaca(placa)).thenReturn(veiculo1);

		Locacao locacao1 = new Locacao(cliente1, veiculos, dataLocacao, quantidadeDiasLocados);
		PowerMockito.whenNew(ArrayList.class).withNoArguments().thenReturn(veiculos);
		PowerMockito.whenNew(Locacao.class).withArguments(cliente1, veiculos, dataLocacao, quantidadeDiasLocados).thenReturn(locacao1);

		PowerMockito.mockStatic(LocacaoDAO.class);

		LocacaoBO.locarVeiculos(cpfCliente,placas,dataLocacao,quantidadeDiasLocados);

		PowerMockito.verifyStatic(LocacaoDAO.class);
		LocacaoDAO.insert(locacao1);
		PowerMockito.verifyNoMoreInteractions(LocacaoDAO.class);

	}
}
