package br.ucsal.testequalidade20192.locadora.builder;

import br.ucsal.testequalidade20192.locadora.dominio.Cliente;

public class ClienteBuilder {

    private static final String CPF_DEFAULT = "666.666.666-00";
    private static final String NOME_DEFAULT = "Cláudio neiva";
    private static final String TELEFONE_DEFAULT = "55-71-230-8420";

    private String cpf = CPF_DEFAULT;
    private String nome  = NOME_DEFAULT;
    private String telefone = TELEFONE_DEFAULT;

    private ClienteBuilder() {
    }

    public static ClienteBuilder umCliente() {
        return new ClienteBuilder();
    }

    private ClienteBuilder comNome (String nome) {
        this.nome = nome;
        return this;
    }

    public ClienteBuilder comCPF (String cpf) {
        this.cpf = cpf;
        return this;
    }

    private ClienteBuilder comTelefone (String telefone) {
        this.telefone = telefone;
        return this;
    }

    private ClienteBuilder mas() {
        return umCliente().comCPF(cpf).comNome(nome).comTelefone(telefone);
    }

    public Cliente build() {
        return new Cliente(cpf,nome,telefone);
    }


}
